import streamlit as st
import streamlit.components.v1 as components

import pandas as pd
import numpy as np

import gspread
from oauth2client.service_account import ServiceAccountCredentials
import json
import pandas as pd
import os

st.title(':nut_and_bolt: Scraper')
st.write('### List of current reports run from application')
st.write("---")


### API key
st.write("## Input API key (_json_)")

upFile=st.file_uploader("Upload file",type=['json'])
if upFile==None:
    st.write("No _json_ file added")
    ### download button
    with open("/code/streamlit/pages/testAPI.json", 'rb') as file:
        st.download_button(
            label="Download example json",
            data=file,
            file_name="testAPI.json",
            mime="json"
            )
    st.stop()
else:
    bytes_data = upFile.getvalue()
    data = upFile.getvalue().decode('utf-8')  
    if "json" in upFile.type.lower():
        ### write tmp file
        with open("/tmp/tmpFile.json","w") as f: 
            f.write(data)
        st.write("### Input json")
        with open("/tmp/tmpFile.json","r") as f: 
            inFile = json.load(f)
            st.json(inFile, expanded=False)
    else:
        st.write(data)

st.write("### Input googleDoc URL")
exampleURL="https://docs.google.com/spreadsheets/d/1TyWA7KHZTfnLsnbZMEa3zpSitU1mz_I6oY_5wzxJVrM/edit#gid=0"
st.write(f"example googleDoc: [link]({exampleURL})")

gdUrl=st.text_input("URL:",value=exampleURL)

st.write("---")

st.write("### Get Data")

### api stuff
# scopes = [
# 'https://www.googleapis.com/auth/spreadsheets',
# 'https://www.googleapis.com/auth/drive'
# ]

### https://practicaldatascience.co.uk/data-science/how-to-read-google-sheets-data-in-pandas-with-gspread
gc = gspread.service_account(filename="/tmp/tmpFile.json",)
sh = gc.open_by_url(gdUrl)
ws = sh.worksheet('Sheet1')

### get data
if st.button("Get data"):

    ### extra hack to avoid expected_headers error, from:https://github.com/burnash/gspread/issues/1007
    expected_headers= ws.row_values(1)
    df_sheet = pd.DataFrame(ws.get_all_records(expected_headers=expected_headers))
try:
    st.dataframe(df_sheet)
except NameError:
    st.write("Get data!")

st.write("---")

st.write("Write Data")

xPos=st.slider("Column:",0, 10, 5)
yPos=st.slider("Row:",0, 10, 5)
txt=st.text_input("Some text:",value="some text")

if st.button("Write data"):
    retVal=ws.update_cell(yPos, xPos, txt)
    st.success(retVal)

### get info to df
